﻿using Microsoft.AspNet.Identity;
using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WarsawEventsMVC.Controllers;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;
using WarsawEventsService.Tests;

namespace WarsawEventsMVC.Tests.Controllers
{
    [TestFixture]
    class ApplicationUsersControllerTest
    {
        [Test]
        public void IndexApplicationUser()
        {
            // var manager = new UserManager<MemoryUser>(new MemoryUserStore());
            //var rep = new MockRepository<Place>();
            ////rep.Insert(new Place());
            // Arrange
            ApplicationUsersController controller = new ApplicationUsersController(new MockRepository<ApplicationUser>(), new MockRepository<Invitation>());

            // Act
            ViewResult result = controller.Index("Wszyscy") as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexApplicationUsersModel()
        {
            // var manager = new UserManager<MemoryUser>(new MemoryUserStore());
            var rep = new MockRepository<ApplicationUser>();
            rep.Insert(new ApplicationUser() { Id = "1" });

            // Arrange
            ApplicationUsersController controller = new ApplicationUsersController(rep, new MockRepository<Invitation>());


            // Act
            ViewResult result = controller.Index("Wszyscy") as ViewResult;
            var model = result.Model as IEnumerable<ApplicationUser>;
            // Assert
            Assert.AreEqual(true, model.Any(u => u.Id == "1"));
        }
    }
}
