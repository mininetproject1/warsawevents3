﻿using Microsoft.AspNet.Identity;
using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WarsawEventsMVC.Controllers;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;
using WarsawEventsService.Tests;

namespace WarsawEventsMVC.Tests.Controllers
{
    [TestFixture]
    class ApplicationUsersInEventControllerTest
    {

        [Test]
        public void IndexApplicationUsersInEvent()
        {
            // var manager = new UserManager<MemoryUser>(new MemoryUserStore());
            //var rep = new MockRepository<Place>();
            ////rep.Insert(new Place());
            // Arrange
            ApplicationUsersInEventController controller = new ApplicationUsersInEventController(new MockRepository<ApplicationUser>(),
                new MockRepository<Event>(), new MockRepository<Participant>());


            // Act
            ViewResult result = controller.Index(0, true ) as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void IndexApplicationUsersInEventModel()
        {
            // var manager = new UserManager<MemoryUser>(new MemoryUserStore());
            
            var repEvent = new MockRepository<Event>();
            var like = new Like() { EventId = 0 };
            var likeList = new List<Like>();
            likeList.Add(like);
            repEvent.Insert(new Event() { Id = 0, Likes= likeList });

            var rep = new MockRepository<ApplicationUser>();
            rep.Insert(new ApplicationUser() { Id = "1", Likes = likeList});

            var repPart = new MockRepository<Like>();
            repPart.Insert(like);

            // Arrange
            ApplicationUsersInEventController controller = new ApplicationUsersInEventController(rep,
                repEvent, new MockRepository<Participant>());


            // Act
            ViewResult result = controller.Index(0, true) as ViewResult;
            var model = result.Model as IEnumerable<ApplicationUser>;
            // Assert
            Assert.AreEqual(true, model.Any(u => u.Id == "1"));
        }
    }
}
