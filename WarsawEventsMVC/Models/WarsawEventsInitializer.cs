﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsMVC.Models
{
    public class WarsawEventsInitializer : DropCreateDatabaseIfModelChanges<WarsawEventsContext>//DropCreateDatabaseAlways<WarsawEventsContext>
    {
        protected override void Seed(WarsawEventsContext context)
        {
            List<Category> categories = new List<Category>();
            List<District> districts = new List<District>();
            List<Place> places = new List<Place>();
            //List<User> users = InitializeUsers(context);
            InitializeCategories(context, categories);
            InitializeDistricts(context, districts);
            InitializePlaces(context, places, districts);
            List<Event> events = InitializeEvents(context,places,categories);
            context.SaveChanges();
            //InitializeGrades(context, places,users[0]);
            //InitializeComments(context, users);
            //context.SaveChanges();

            base.Seed(context);
        }

        /*private void InitializeComments(WarsawEventsContext context, List<User> users)
        {
            List<Comment> comments = new List<Comment>();
            DateTime date= DateTime.Now;
            comments.Add(new Comment() { Date = date, EventId = 2, User = users[0], Text = "Super wydarzenie" });
            comments.Add(new Comment() { Date = date.AddHours(1), EventId = 2, User = users[1], Text = "Sprzedam Opla" });
            comments.Add(new Comment() { Date = date.AddMinutes(2), EventId = 1, User = users[0], Text = "Polecam tego allegrowicza" });
            comments.Add(new Comment() { Date = date.AddMinutes(3), EventId = 3, User = users[1], Text = "Fajne wydarzenie" });
            comments.Add(new Comment() { Date = date.AddMinutes(4), EventId = 4, User = users[2], Text = "Nie mogę się doczekać" });
            comments.Add(new Comment() { Date = date.AddMinutes(5), EventId = 5, User = users[3], Text = "I tak nikt nie czyta tych komentów" });
            comments.Add(new Comment() { Date = date.AddMinutes(6), EventId = 6, User = users[0], Text = "Zapraszam" });
            comments.Add(new Comment() { Date = date.AddMinutes(7), EventId = 7, User = users[1], Text = "I tak nikt nie czyta tych komentów" });
        }*/

        private void InitializePlaces(WarsawEventsContext context, List<Place> places, List<District> districts)
        {
            District srodmiescie = districts[0];
            District wola = districts[1];

            places.Add(new Place() { District=srodmiescie, PlaceName = "Klub", StreetName = "Marszałkowska", Streetnumber = 69 });
            places.Add(new Place() { District = srodmiescie, PlaceName = "Klub Hybrydy", StreetName = "Złota", Streetnumber = 7 });
            places.Add(new Place() { District = srodmiescie, PlaceName = "MiNI PW", StreetName = "Koszykowa", Streetnumber = 75 });
            places.Add(new Place() { District = srodmiescie, PlaceName = "Stadion Miejski", StreetName = "Łazienkowska", Streetnumber = 3 });
            places.Add(new Place() { District = srodmiescie, PlaceName = "Centrum Nauki Kopernik", StreetName = "Wybrzeże Kościuszkowskie", Streetnumber = 20 });
            places.Add(new Place() { District  =wola, PlaceName = "Muzeum Powstania Warszawskiego", StreetName = "Grzybowska", Streetnumber = 79 });
            places.Add(new Place() { District = wola, PlaceName = "DS Ustronie", StreetName = "Księcia Janusza", Streetnumber = 39 });
            places.Add(new Place() { District = wola, PlaceName = "Wolskie Centrum Kultury", StreetName = "Obozowa", Streetnumber = 85 });

            places.ForEach(p => context.Places.Add(p));
        }

        /*private void InitializeGrades(WarsawEventsContext context, List<Place> places, User u)
        {
            Random rand = new Random();
            places.ForEach(p => context.Grades.Add(new Grade() { PlaceId = p.Id, UserId = u.Id, Value = rand.Next(1, 5) }));
        }*/

        private List<Event> InitializeEvents(WarsawEventsContext context, List<Place> places, List<Category> categories)
        {
            List<Event> events = new List<Event>();

            events.Add(new Event()
            {
                Category = categories[0],
                Place = places[2],
                EventName = "Wykład",
                Description = "Wykład z .Net. Tematem wykładu będzie wzorzec MVVM."
                ,
                Price = 0,
                NumberOfParticipants = 90,
                DateOfStart = new DateTime(2017, 11, 15, 16, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[0],
                Place = places[2],
                EventName = "Wykład",
                Description = "Tematem wykładu będzie prawdopodobieństwo warunkowe.",
                Price = 0,
                NumberOfParticipants = 90,
                DateOfStart = new DateTime(2017, 11, 15, 12, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[0],
                Place = places[4],
                EventName = "Minilaby",
                Description = "Rozpalone do białości, minilab fizyczny.",
                Price = 25,
                NumberOfParticipants = 10,
                DateOfStart = new DateTime(2017, 11, 15, 12, 45, 0)
            });
            events.Add(new Event()
            {
                Category = categories[0],
                Place = places[2],
                EventName = "Wykład",
                Description = "Wykład z fizyki. Tematem wykładu będzie elektrostatyka.",
                Price = 0,
                NumberOfParticipants = 90,
                DateOfStart = new DateTime(2017, 11, 16, 16, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[0],
                Place = places[4],
                EventName = "Minilaby",
                Description = "Świecąca meduza, minilab biologiczny.",
                Price = 25,
                NumberOfParticipants = 10,
                DateOfStart = new DateTime(2017, 11, 18, 12, 45, 0)
            });
            events.Add(new Event()
            {
                Category = categories[2],
                Place = places[1],
                EventName = "Otrzęsiny wydziału MiNI",
                Description = "Co roku organizujemy otrzęsiny wydziału MiNI.",
                Price = 15,
                NumberOfParticipants = 72,
                DateOfStart = new DateTime(2017, 11, 12, 19, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[1],
                Place = places[3],
                EventName = "Mecz Legia - Real Madryt",
                Description = "Oczekujemy gorącego wsparcia kibiców.",
                Price = 15,
                NumberOfParticipants = 72,
                DateOfStart = new DateTime(2017, 11, 12, 19, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[7],
                Place = places[4],
                EventName = "Wystawa",
                Description = "Zaprezentujemy nową kolekcje...",
                Price = 25,
                NumberOfParticipants = 20,
                DateOfStart = new DateTime(2017, 11, 11, 11, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[8],
                Place = places[5],
                EventName = "Noc muzeów",
                Description = "Zchęcamy gorąco...",
                Price = 0,
                NumberOfParticipants = 2000,
                DateOfStart = new DateTime(2017, 11, 10, 23, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[2],
                Place = places[6],
                EventName = "Ustronalia",
                Description = "Nie może Ciebie zabraknąć.",
                Price = 5,
                NumberOfParticipants = 200,
                DateOfStart = new DateTime(2017, 11, 18, 23, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[2],
                Place = places[6],
                EventName = "Ustro-Wózki",
                Description = "Spróbuj swoich sił w nowej dyscyplinie sportowej.",
                Price = 1,
                NumberOfParticipants = 20,
                DateOfStart = new DateTime(2017, 11, 18, 22, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[3],
                Place = places[7],
                EventName = "Koncert T.Love",
                Description = "Nie może Ciebie zabraknąć.",
                Price = 18,
                NumberOfParticipants = 250,
                DateOfStart = new DateTime(2017, 11, 12, 20, 15, 0)
            });
            events.Add(new Event()
            {
                Category = categories[4],
                Place = places[7],
                EventName = "Premiera filmu Wołyń",
                Description = "Szokujący film ukazujący...",
                Price = 10,
                NumberOfParticipants = 50,
                DateOfStart = new DateTime(2017, 11, 12, 20, 0, 0)
            });
            events.Add(new Event()
            {
                Category = categories[6],
                Place = places[7],
                EventName = "Premiera spektaklu Niepoprawni",
                Description = "Niesamowity debiut...",
                Price = 40,
                NumberOfParticipants = 40,
                DateOfStart = new DateTime(2017, 11, 12, 20, 0, 0)
            });
            events.Add(new Event()
            {
                Category = categories[7],
                Place = places[4],
                EventName = "Warsztaty familijne",
                Description = "Dlaczego statki pływają? Dlaczego słońce świeci?",
                Price = 25,
                NumberOfParticipants = 220,
                DateOfStart = new DateTime(2017, 11, 15, 11, 15, 0)
            });

            events.ForEach(e => context.Events.Add(e));
            return events;
        }

        private void InitializeDistricts(WarsawEventsContext context, List<District> districts)
        {
            districts.Add(new District() { DistrictName = "Śródmieście" });
            districts.Add(new District() { DistrictName = "Wola" });
            districts.Add(new District() { DistrictName = "Bemowo" });
            districts.Add(new District() { DistrictName = "Praga" });
            districts.Add(new District() { DistrictName = "Żoliborz" });
            districts.Add(new District() { DistrictName = "Mokotów" });
            districts.Add(new District() { DistrictName = "Ursynów" });
            districts.Add(new District() { DistrictName = "Ochota" });

            districts.ForEach(d => context.Districts.Add(d));
        }

        private void InitializeCategories(WarsawEventsContext context, List<Category> categories)
        {
            categories.Add(new Category() { CategoryName = "Nauka" });
            categories.Add(new Category() { CategoryName = "Sport" });
            categories.Add(new Category() { CategoryName = "Rozrywka" });
            categories.Add(new Category() { CategoryName = "Muzyka" });
            categories.Add(new Category() { CategoryName = "Film" });
            categories.Add(new Category() { CategoryName = "Literatura" });
            categories.Add(new Category() { CategoryName = "Teatr" });
            categories.Add(new Category() { CategoryName = "Wystawa" });
            categories.Add(new Category() { CategoryName = "Dla dzieci" });

            categories.ForEach(c=>context.Categorys.Add(c));
        }

        /*private List<User> InitializeUsers(WarsawEventsContext context)
        {
            string pass="admin1";
            using(MD5 md5hash = MD5.Create())
            {
                pass = String.Join
                (
                    "",
                    from ba in md5hash.ComputeHash
                    (
                        Encoding.UTF8.GetBytes(pass)
                    )
                    select ba.ToString("x2")
                );
            }
            List<User> users = new List<User>();
            for (int i = 0; i < 10; i++)
            {
                User admin = new User() { UserName = "admin"+i, Password = pass };
                users.Add(admin);
            }
            users.ForEach(u => context.Users.Add(u));
            return users;
        }*/
    }
}
