﻿using System.ComponentModel.DataAnnotations;

namespace WarsawEventsMVC.Models.Entities
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
