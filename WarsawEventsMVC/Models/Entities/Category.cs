﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVC.Models.Entities
{
    public class Category: Entity
    {
        
        public string CategoryName { get; set; }

        public virtual ICollection<Event> Events { get; set; }
    }
}
