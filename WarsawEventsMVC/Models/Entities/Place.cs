﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WarsawEventsMVC.Models.Entities
{
    public class Place: Entity
    {
        [Display(Name = "Nazwa miejsca")]
        [Required(ErrorMessage = "Podaj nazwe miejsca")]
        [StringLength(30)]
        public string PlaceName { get; set; }

        [Display(Name = "Ulica")]
        [Required(ErrorMessage = "Podaj nazwe ulicy")]
        [StringLength(30)]
        public string StreetName { get; set; }

        [Display(Name = "Numer domu")]
        [Required(ErrorMessage = "Podaj numer domu")]
        public int Streetnumber { get; set; }

        [Display(Name = "Ocena")]
        [ScaffoldColumn(false)]
        public double Average { get; set; }

        //Foreign key for District
        public int DistrictId { get; set; }
        public virtual District District { get; set; }
        public virtual ICollection<Grade> Grades { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}
