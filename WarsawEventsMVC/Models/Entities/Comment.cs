﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarsawEventsMVC.Models.Entities
{
    public class Comment:Entity
    {
        public int EventId { get; set; }
        public string ApplicationUserId { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }

        public virtual Event Event { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
