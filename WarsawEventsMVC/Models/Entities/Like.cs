﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVC.Models.Entities
{
    public class Like
    {
        [Key]
        [Column(Order = 0)]
        public int EventId { get; set; }
        [Key]
        [Column(Order = 1)]
        public string ApplicationUserId { get; set; }

        public virtual Event Event { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
