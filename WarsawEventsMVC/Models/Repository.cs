﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVC.Models
{
    public interface IRepository<TEntity> :IDisposable
    {
        ICollection<TEntity> GetAll();
        TEntity Insert(TEntity tEntity);
        void Delete(int Id);
        void Delete(TEntity concreteTEntity);
        void Update(TEntity tEntity);
        TEntity Find(int id);
        IQueryable<TEntity> Get();

        void Save();
    }

    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity: class
    {
        WarsawEventsContext context;
        public Repository(WarsawEventsContext context)
        {
            this.context = context;
        }

        public ICollection<TEntity> GetAll()
        {
            return context.Set<TEntity>().ToList<TEntity>();  
        }

        public TEntity Find(int id)
        {
            return context.Set<TEntity>().Find(id);
        }

        public TEntity Insert(TEntity newTEntity)
        {
            return context.Set<TEntity>().Add(newTEntity);
        }

        public void Delete(int Id)
        {
            TEntity concreteTEntity = context.Set<TEntity>().Find(Id);
            context.Set<TEntity>().Remove(concreteTEntity);
        }

        public void Delete(TEntity concreteTEntity)
        {
            context.Set<TEntity>().Remove(concreteTEntity);
        }

        public void Update(TEntity updatedTEntity)
        {
            context.Entry(updatedTEntity).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }



        public IQueryable<TEntity> Get()//pozwala robic includa
        {
            return context.Set<TEntity>();            
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
