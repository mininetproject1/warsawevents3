﻿using System.Data.Entity;
using System;
using WarsawEventsMVC.Models.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WarsawEventsMVC.Models
{

    public class WarsawEventsContext : IdentityDbContext<ApplicationUser>, IDisposable
    {

        public DbSet<Category> Categorys { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Participant> Participants { get; set; }

        public WarsawEventsContext() : base("name=WarsawEventsConnectionString")
        {   //dropuje db gdy zmieni sie model
            Database.SetInitializer(new WarsawEventsInitializer());

            this.Configuration.LazyLoadingEnabled = false;
           // this.Configuration.ProxyCreationEnabled = false; //nie wywala przy serializacji obiektu
        }

        public static WarsawEventsContext Create()
        {
            return new WarsawEventsContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //many-to-one
            modelBuilder.Entity<ApplicationUser>()
                       .HasMany(u => u.SendInvitations)
                       .WithRequired()
                       .HasForeignKey(i => i.SendUserId).WillCascadeOnDelete(false);

            //many-to-one 
            modelBuilder.Entity<ApplicationUser>()
                        .HasMany(u => u.ReceiveInvitations)
                        .WithRequired()
                        .HasForeignKey(i => i.ReceiveUserId).WillCascadeOnDelete(false);

        }

   

        // public System.Data.Entity.DbSet<WarsawEventsMVC.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}
