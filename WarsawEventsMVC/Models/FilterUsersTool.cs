﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsMVC.Models
{
    public class FilterUsersTool
    {
        public static List<string> GetUserList(string s, ApplicationUser user)
        {



            List<string> userList = new List<string>();
            if (s != null)
                s = s.ToLower();
            else
                return null;

            if (user == null)
            {
                if (s == null || s == "wszyscy")
                    return null;
                else
                    return userList;
            }


            switch (s)
            {
                case "zaproszeni":
                    if (user.SendInvitations != null)
                        foreach (Invitation inv in user.SendInvitations)
                            if (inv.IsAccepted == null || inv.IsAccepted == false)
                                userList.Add(inv.ReceiveUserId);
                    return userList;

                case "zapraszający":
                    if (user.ReceiveInvitations != null)
                        foreach (Invitation inv in user.ReceiveInvitations)
                            if (inv.IsAccepted == null || inv.IsAccepted == false)
                                userList.Add(inv.SendUserId);
                    return userList;

                case "znajomi":
                    if (user.ReceiveInvitations != null)
                        foreach (Invitation inv in user.ReceiveInvitations)
                        {
                            if (inv.IsAccepted != null && inv.IsAccepted == true)
                                userList.Add(inv.SendUserId);
                        }
                    if (user.SendInvitations != null)
                        foreach (Invitation inv in user.SendInvitations)
                        {
                            if (inv.IsAccepted != null && inv.IsAccepted == true)
                                userList.Add(inv.ReceiveUserId);
                        }
                    return userList;



                default:
                    return null;
            }//switch

        }
    }//class
}