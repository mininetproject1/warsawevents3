﻿using Microsoft.AspNet.Identity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsMVC.Controllers
{
    public class EventsController : Controller
    {
        
        private IEventRepository repoEvent;
        private IRepository<Category> repoCatgory;
        private IRepository<District> repoDistrict;
        private IRepository<Place> repoPlace;
        private IRepository<Participant> repoParticipant;
        private IRepository<Like> repoLike;
        private IRepository<Invitation> repoInvitation;
        private IRepository<Comment> repoComment;

        public EventsController(IEventRepository repoEvent, IRepository<Category> repoCatgory, IRepository<District> repoDistrict,IRepository<Place> repoPlace, IRepository<Participant> repoParticipant, IRepository<Like> repoLike,IRepository<Invitation> repoInvitation, IRepository<Comment> repoComment)//zmienić!! kiedys na dependency injection
        {

            this.repoEvent = repoEvent;
            this.repoCatgory = repoCatgory;
            this.repoPlace = repoPlace;
            this.repoDistrict = repoDistrict;
            this.repoParticipant = repoParticipant;
            this.repoLike = repoLike;
            this.repoInvitation = repoInvitation;
            this.repoComment = repoComment;
        }

        // GET: Events
        public ActionResult Index(string eventsFilter, string district, string category, string period, string currentSort, string currentFilter, string searchString, int? page)
        {
            if (Request != null && Request.IsAjaxRequest() == false)
            {
                var categories = repoCatgory.Get().ToList(); //naprawic wysukaj
                categories.Add(new Category() { CategoryName = "Wszystkie Kategorie" });
                category = string.IsNullOrWhiteSpace(category) ? "Wszystkie Kategorie" : category;
                ViewBag.Categories = new SelectList(categories, "CategoryName", "CategoryName", category);

                var districts = repoDistrict.Get().ToList();
                districts.Add(new District() { DistrictName = "Wszystkie Dzielince" });
                district = string.IsNullOrWhiteSpace(district) ? "Wszystkie Dzielince" : district;
                ViewBag.Districts = new SelectList(districts, "DistrictName", "DistrictName", district);
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);
            currentSort = (currentSort ?? "Dacie");
            period = (period ?? "Wszystkie");
            ViewBag.CurrentSort = currentSort;
            ViewBag.CurrentPeriod = period;
            ViewBag.CurrentCategory = category;
            ViewBag.CurrentDistrict = district;
            ViewBag.CurrentEventsFilter = eventsFilter;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;//przypadek gdy chcemy wyswietlic druga str 
            }
            ViewBag.CurrentFilter = searchString;

            List<Expression<Func<Event, bool>>> funcList = new List<Expression<Func<Event, bool>>>();
            if (string.IsNullOrWhiteSpace(searchString) == false)
                funcList.Add((Event e) => e.EventName.Contains(searchString) || e.Place.PlaceName.Contains(searchString) || e.Place.District.DistrictName.Contains(searchString)
                       || e.Description.Contains(searchString) || e.Place.StreetName.Contains(searchString)
                       || e.Category.CategoryName.Contains(searchString));
            funcList.Add(FuncTool.GetPeriodFunc(period));
            if (Request != null && Request.IsAuthenticated)
            {
                string userId = User.Identity.GetUserId();
                List<string> friends = null;
                if (eventsFilter == "Polecane")
                {
                    friends = repoInvitation.Get().Where(i => i.ReceiveUserId == userId && i.IsAccepted == true).Select(i => i.SendUserId).ToList();
                    friends.AddRange(repoInvitation.Get().Where(i => i.SendUserId == userId && i.IsAccepted == true).Select(i => i.ReceiveUserId).ToList());
                }
                funcList.Add(FuncTool.GetTypeOfEventsFunc(eventsFilter, userId, friends));
            }
            if (category != "Wszystkie Kategorie")
                funcList.Add(e => e.Category.CategoryName == category);
            if (district != "Wszystkie Dzielince")
                funcList.Add(e => e.Place.District.DistrictName == district);

            var events = repoEvent.GetEvents(funcList)
                .Include(c => c.Category)
                .Include(p => p.Place)
                .OrderBy(FuncTool.GetSortFunc(currentSort))
                .Select(e => (EventViewModel)e);

            var pageEvents = events.ToPagedList(pageNumber, pageSize);
            IncludeComments(pageEvents);

            if (Request != null && Request.IsAuthenticated)
            {
                string userId = User.Identity.GetUserId();
                foreach(var ev in pageEvents)
                {
                    Participant part = repoParticipant.Get().Where(p => p.EventId == ev.Id && p.ApplicationUserId == userId).FirstOrDefault();
                    if (part != null)
                    {
                        ev.IsParticipant = true;
                        ev.IsOwner = (part.IsOwner ?? false);
                    }
                    Like like = repoLike.Get().Where(l => l.ApplicationUserId == userId && l.EventId == ev.Id).FirstOrDefault();
                    ev.IsLike = like != null ? true : false;
                }
            }

            if (Request != null && Request.IsAjaxRequest())
                return PartialView("_Events", pageEvents);

            return View(pageEvents);
        }

        // GET: Events/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(repoCatgory.GetAll(), "Id", "CategoryName");
            ViewBag.DistrictId = new SelectList(repoDistrict.GetAll(), "Id", "DistrictName");
            return View();
        }

        public JsonResult GetPlaceNames(string term)
        {
            List<string> names = repoPlace.Get().Where(p => p.PlaceName.Contains(term)).GroupBy(p => p.PlaceName)
                                .Take(4).Select(p => p.Key).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStreetNames(string term)
        {
            List<string> names = repoPlace.Get().Where(p => p.StreetName.Contains(term)).GroupBy(p=>p.StreetName)
                                .Take(4).Select(p => p.Key).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EventName,Description,Price,DateOfStart,PlaceId,CategoryId,Place")] Event @event, int DistrictId)
        {
            if (ModelState.IsValid)
            {
                Place place= repoPlace.Get().Where(p => p.DistrictId == DistrictId && p.PlaceName.ToLower() == @event.Place.PlaceName.Trim().ToLower()
                             && p.StreetName.ToLower() == @event.Place.StreetName.Trim().ToLower() && p.Streetnumber == @event.Place.Streetnumber)
                             .FirstOrDefault();

                @event.Place.DistrictId = DistrictId;
                @event.NumberOfParticipants = 1;

                if (place != null)
                {
                    @event.Place = null;
                    @event.PlaceId = place.Id;
                }
                Event newEvent = repoEvent.InsertEvent(@event);
                repoEvent.Save();

                repoParticipant.Insert(new Participant()
                {
                    ApplicationUserId = User.Identity.GetUserId(),
                    EventId = newEvent.Id,
                    IsOwner=true
                });
                repoParticipant.Save();

                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(repoCatgory.GetAll(), "Id", "CategoryName", @event.CategoryId);
            ViewBag.DistrictId = new SelectList(repoDistrict.GetAll(), "Id", "DistrictName", @event.Place.DistrictId);
            return View(@event);
        }

        // GET: Events/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = repoEvent.GetCurrentEvents().Where(e=>e.Id==id)
                .Include(e=>e.Category)
                .Include(e=>e.Place).FirstOrDefault();
            if (@event == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(repoCatgory.GetAll(), "Id", "CategoryName", @event.CategoryId);
            ViewBag.DistrictId = new SelectList(repoDistrict.GetAll(), "Id", "DistrictName", @event.Place.DistrictId);
            return View(@event);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EventName,Description,Price,DateOfStart,PlaceId,Place,CategoryId")] Event @event, int DistrictId)
        {
            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                Participant part = repoParticipant.Get().Where(p => p.ApplicationUserId == currentUserId
                                     && p.EventId == @event.Id && p.IsOwner == true).FirstOrDefault();
                if (part == null)//spr czy jest wlascicielem wydarzenia
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

                Place place = repoPlace.Get().Where(p => p.DistrictId == DistrictId && p.PlaceName.ToLower() == @event.Place.PlaceName.Trim().ToLower()
                              && p.StreetName.ToLower() == @event.Place.StreetName.Trim().ToLower() && p.Streetnumber == @event.Place.Streetnumber)
                             .FirstOrDefault();

                @event.Place.DistrictId = DistrictId;
                if (place == null)
                {
                    place = repoPlace.Insert(@event.Place);
                    repoPlace.Save();
                }                  
                
                @event.Place = null;
                @event.PlaceId = place.Id;
                repoEvent.UpdateEvent(@event);
                repoEvent.Save();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(repoCatgory.GetAll(), "Id", "CategoryName", @event.CategoryId);
            ViewBag.DistrictId = new SelectList(repoDistrict.GetAll(), "Id", "DistrictName", @event.Place.DistrictId);
            return View(@event);
        }

        // GET: Events/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Event @event = repoEvent.GetCurrentEvents().Where(e => e.Id == id)
                .Include(e => e.Category)
                .Include(e => e.Place).FirstOrDefault();

            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            string currentUserId = User.Identity.GetUserId(); //sprawdzenie czy jest właścicielem wydarzenia
            Participant part= repoParticipant.Get().Where(p => p.ApplicationUserId == currentUserId
                                    && p.EventId == id && p.IsOwner == true).FirstOrDefault();
            if(part==null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            repoEvent.DeleteEvent(id);
            repoEvent.Save();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize]
        public ActionResult SendComment(string text,string eventsFilter, int page, int id, string district, string category, string period, string currentSort, string currentFilter, string searchString)
        {
            Comment comment = new Comment()
            {
                ApplicationUserId = User.Identity.GetUserId(),
                EventId = id,
                Text = text,
                Date= DateTime.Now
            };

            repoComment.Insert(comment);
            repoComment.Save();

            return RedirectToAction("Index", new { eventsFilter, district, category, period, currentSort, currentFilter, searchString, page });
        }

        [HttpPost]
        [Authorize]
        public ActionResult Like(string eventsFilter, int page, int id, string district, string category, string period, string currentSort, string currentFilter, string searchString)
        {
            string currentUser = User.Identity.GetUserId();
            Like like = repoLike.Get().Where(l => l.ApplicationUserId == currentUser && l.EventId == id).FirstOrDefault();
            Event ev = repoEvent.Find(id); //spr czy istn taki event
            if (like != null || ev==null)//juz lajkował
                return Index(eventsFilter, district, category, period, currentSort, currentFilter, searchString, page);
            repoLike.Insert(new Like()
            {
                ApplicationUserId = currentUser,
                EventId = id
            });
            repoLike.Save();

            return RedirectToAction("Index", new { eventsFilter, district, category, period, currentSort, currentFilter, searchString, page });
        }

        [HttpPost]
        [Authorize]
        public ActionResult Enroll(string eventsFilter, int page, int id, string district, string category, string period, string currentSort, string currentFilter, string searchString)
        {
            string currentUser = User.Identity.GetUserId();
            Participant partic = repoParticipant.Get().Where(l => l.ApplicationUserId == currentUser && l.EventId == id).FirstOrDefault();
            if (partic != null)//juz dolaczyl
                return Index(eventsFilter, district, category,period,currentSort,currentFilter,searchString,page);
            repoParticipant.Insert(new Participant()
            {
                ApplicationUserId = currentUser,
                EventId = id,
                IsOwner=false
            });
            repoParticipant.Save();

            Event ev = repoEvent.Find(id);//mozna poprawic by nie przeoczyc updatu
            ev.NumberOfParticipants++;
            repoEvent.UpdateEvent(ev);
            repoEvent.Save();
            return RedirectToAction("Index", new { eventsFilter, district, category, period, currentSort, currentFilter, searchString, page });
        }

        [HttpPost]
        [Authorize]
        public ActionResult Resign(string eventsFilter, int page, int id, string district, string category, string period, string currentSort, string currentFilter, string searchString)
        {
            string currentUser = User.Identity.GetUserId();
            Participant partic = repoParticipant.Get().Where(l => l.ApplicationUserId == currentUser && l.EventId == id).FirstOrDefault();
            if (partic == null || partic.IsOwner.Value)//juz zrezygnowal, albo jest wlascicielem
                return Index(eventsFilter, district, category, period, currentSort, currentFilter, searchString, page);
            repoParticipant.Delete(partic);
            repoParticipant.Save();

            Event ev = repoEvent.Find(id);//mozna poprawic by nie przeoczyc updatu
            ev.NumberOfParticipants--;
            repoEvent.UpdateEvent(ev);
            repoEvent.Save();
            return RedirectToAction("Index", new { eventsFilter, district, category, period, currentSort, currentFilter, searchString, page });
        }

        private void IncludeComments(IPagedList<EventViewModel> pageEvents)
        {
            foreach (var ev in pageEvents)
                ev.Comments = repoComment.Get().Where(c => c.EventId == ev.Id).Include(c => c.ApplicationUser).ToList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repoEvent.Dispose();
                repoCatgory.Dispose();
                repoComment.Dispose();
                repoDistrict.Dispose();
                repoInvitation.Dispose();
                repoLike.Dispose();
                repoParticipant.Dispose();
                repoPlace.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
