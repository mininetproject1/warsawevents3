﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WarsawEventsMVC.Models;
using WarsawEventsMVC.Models.Entities;

namespace WarsawEventsMVC.Controllers
{
    public class ApplicationUsersInEventController : Controller
    {
        private WarsawEventsContext db = new WarsawEventsContext();

        // GET: ApplicationUsers
        IRepository<ApplicationUser> repUser;
        IRepository<Event> repEvent;
        IRepository<Participant> repParticipant;

        public ApplicationUsersInEventController(IRepository<ApplicationUser> repUser,
        IRepository<Event> repEvent, IRepository<Participant> repParticipant)
        {
            this.repUser = repUser;
            this.repEvent = repEvent;
            this.repParticipant = repParticipant;
        }
        // GET: ApplicationUsersInEvent
        public ActionResult Index(int eventId, bool like)
        {
            if (like == true)
                return View(repUser.Get().Include(u => u.Likes).Where(u => u.Likes.Any(l => l.EventId == eventId)));
            else
            {
                string userId = User.Identity.GetUserId();
                Event curEvent = repEvent.Get().Include(e => e.Participants).Where(e => e.Id == eventId).FirstOrDefault();
                if(curEvent.Participants.Any(p => p.IsOwner != null && p.IsOwner == true && p.ApplicationUserId == userId ))
                ViewBag.IsOwner = true;
                ViewBag.EventId = eventId;
                return View(repUser.Get().Include(u => u.Likes).Where(u => u.Participants.Any(p => p.EventId == eventId)));
            }

        }

        public ActionResult Eject(string id, int eventId)
        {
            string userId = User.Identity.GetUserId();
            Event curEvent = repEvent.Get().Include(e => e.Participants).Where(e => e.Id == eventId).FirstOrDefault();
            if (curEvent.Participants.Any(p => p.IsOwner != null && p.IsOwner == true && p.ApplicationUserId == userId))
            {
                ViewBag.IsOwner = true;
                ViewBag.EventId = eventId;
                Participant participant = repParticipant.Get().Where(p => p.EventId == eventId && p.ApplicationUserId == id).FirstOrDefault();
                repParticipant.Delete(participant);
                repParticipant.Save();
            }

            return View("~/Views/ApplicationUsersInEvent/Index.cshtml",
                repUser.Get().Include(u => u.Likes).Where(u => u.Participants.Any(p => p.EventId == eventId)));
        }        
   
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
